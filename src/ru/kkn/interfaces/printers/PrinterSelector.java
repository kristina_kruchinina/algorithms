package ru.kkn.interfaces.printers;

/**
 * Класс для выбора принтера
 *
 * @author K.Kruchinina, 17it17
 */

public class PrinterSelector {

    public IPrinter getPrinter(TypeOfPrinters type) {
        IPrinter printer = null;
        switch (type) {
            case DECORATIVE_PRINTER:
                printer = new DecorativePrinter();
                break;
            case CONSOLE_PRINTER:
                printer = new ConsolePrinter();
                break;
            case ADV_CONSOLE_PRINTER:
                printer = new AdvConsolePrinter();
                break;
        }
        return printer;
    }
}