package ru.kkn.interfaces.printers;

/**
 * Декоративный принтер
 *
 * @author K.Kruchinina, 17it17
 */

public class DecorativePrinter implements IPrinter {

    @Override
    public void print(String text) {
        System.out.println( " " );
        System.out.println( text );
        System.out.println( " " );
    }
}