package ru.kkn.interfaces.printers;

/**
 * Список принтеров
 *
 * @author K.Kruchinina, 17it17
 */

public enum TypeOfPrinters {ADV_CONSOLE_PRINTER, CONSOLE_PRINTER, DECORATIVE_PRINTER}
