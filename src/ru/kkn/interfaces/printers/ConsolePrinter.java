package ru.kkn.interfaces.printers;

/**
 * Консольный принтер
 *
 * @author K.Kruchinina, 17it17
 */

public class ConsolePrinter implements IPrinter {

    @Override
    public void print(String text) {
        System.out.println(text);
    }
}
