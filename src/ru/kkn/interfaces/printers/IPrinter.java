package ru.kkn.interfaces.printers;

/**
 * Принтер
 *
 * @author K.Kruchinina, 17it17
 */

public interface IPrinter {
    void print(final String text);
}
