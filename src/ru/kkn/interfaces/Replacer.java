package ru.kkn.interfaces;

import ru.kkn.interfaces.printers.IPrinter;
import ru.kkn.interfaces.readers.IReader;

/**
 *Реплейсер смайликов
 *
 * @author K.Kruchinina, 17it17
 */

class Replacer {
    private IReader reader;
    private IPrinter printer;

    Replacer(IReader reader, IPrinter printer) {
        this.reader = reader;
        this.printer = printer;
    }

    void replace(){
        final String text = reader.read();
        final String replacedText = text.replaceAll(":\\d+", ":-)");
        printer.print(replacedText);
    }
}
