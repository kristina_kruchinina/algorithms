package ru.kkn.interfaces.readers;

/**
 * Интерфейс для считывания
 *
 * @author K.Kruchinina, 17it17
 */

public interface IReader {
    String read();
}
