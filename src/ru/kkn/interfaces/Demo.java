package ru.kkn.interfaces;

import ru.kkn.interfaces.printers.AdvConsolePrinter;
import ru.kkn.interfaces.printers.ConsolePrinter;
import ru.kkn.interfaces.printers.IPrinter;
import ru.kkn.interfaces.readers.IReader;
import ru.kkn.interfaces.readers.PredefinedReader;

/**
 * Демострация работоспосбности интерфейсов
 *
 * @author K.Kruchinina, 17it17
 */

public class Demo {
    public static void main(String[] args) {
        IReader reader = new PredefinedReader( "Привет :) Пока :)" );
        IPrinter printer = new ConsolePrinter();
        IPrinter advPrinter = new AdvConsolePrinter();
        Replacer replacer = new Replacer( reader, printer );
        Replacer advReplacer = new Replacer( reader, advPrinter );
        replacer.replace();
        advReplacer.replace();
    }
}
