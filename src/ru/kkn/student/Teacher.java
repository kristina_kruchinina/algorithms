package ru.kkn.student;

/**
 * Класс для представления учителя
 *
 * @author K.Kruchinina, 17it17
 */

public class Teacher extends Person {
    private int experience;

    Teacher(String surname, int year, int experience) {
        super( surname, year );
        this.experience = experience;
    }


    Teacher() {
        this( "неизвестно", 0, 0 );
    }

     void setExperience(int experience) {
        this.experience = experience;
    }

    @Override
    public String toString() {
        return "surname = " + getSurname() + ", year = " + getAge() + ", experience = " + experience;
    }
}
