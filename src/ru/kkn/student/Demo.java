package ru.kkn.student;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Класс для представления работоспособностии Person, Student, Teacher
 *
 * @author K.Kruchinina, 17it17
 */

public class Demo {
    private static Scanner scanner = new Scanner( System.in );

    public static void main(String[] args) {
        ArrayList <Person> students = new ArrayList <>();
        students.add( new Student( "Сидоров", 2001, "16ИТ16", 2016, "09.02.07" ) );
        students.add( new Student( "Иванов", 2001, "17ИТ17", 2017, "09.02.07" ) );
        students.add( new Student( "Сергеева", 2000, "15ИТ15", 2015, "09.02.04" ) );
        ArrayList <Person> teachers = new ArrayList <>();
        teachers.add( new Teacher( "Акимов", 1980, 15 ) );
        teachers.add( new Teacher( "Волков", 1965, 30 ) );
        teachers.add( new Teacher( "Абрамов", 1970, 27 ) );

        sortingSurname( students );
        enterTheNumberOfStudents();
        searchSpecialtyCode(students);
        listOutput( students );

        sortingSurname( teachers );
        enterTheNumberOfTeachers();
        listOutput( teachers );

    }

    /**
     * Поиск студентов по коду специальности
     *
     * @param students ссылка на ArrayList
     */
    private static void searchSpecialtyCode(List<Person> students) {
        System.out.print( "Поиск студентов по коду специальности. \nВведите код: " );
        String code = scanner.next();
        for (int i = 0; i< students.size(); i++) {
            if (students.equals( code )) {
                System.out.println( students );
            } else {
                System.out.println( "Такой специальности нет!" );
            }
        }

    }

    /**
     * Сортирует масиив{@code ArrayList}
     * по алфавиту
     *
     * @param persons ссылка на ArrayList
     */

    private static void sortingSurname(List <Person> persons) {
        for (int out = persons.size() - 1; out > 0; out--) {
            for (int in = 0; in < out; in++) {
                if (persons.get( in + 1 ).getSurname().compareTo( persons.get( in ).getSurname() ) < 0) {
                    Person temp = persons.get( in );
                    persons.set( in, persons.get( in + 1 ) );
                    persons.set( in + 1, temp );
                }
            }
        }
    }


    /**
     * Вывод списка
     *
     * @param list список
     */

    private static void listOutput(ArrayList <Person> list) {
        for (Person s : list) {
            System.out.println( s );
        }
    }

    private static void enterTheNumberOfStudents() {
        System.out.print( "Введите количество студентов: " );
        int amount = scanner.nextInt();
        for (int i = 0; i < amount; i++) {
            dataInputStudents();
        }
    }

    private static void dataInputStudents() {
        System.out.print( "Введите фамилию: " );
        String surname = scanner.next();
        System.out.print( "Введите год рождения: " );
        int year = scanner.nextInt();
        System.out.print( "Введите группу: " );
        String group = scanner.next();
        System.out.print( "Введите год поступления: " );
        int admissionYear = scanner.nextInt();
        System.out.print( "Введите код специальности: " );
        String specialtyCode = scanner.next();
        new Student( surname, year, group, admissionYear, specialtyCode );
    }

    private static void enterTheNumberOfTeachers() {
        System.out.print( "Введите количество учителей: " );
        int amount = scanner.nextInt();
        for (int i = 0; i < amount; i++) {
            dataInputTeachers();
        }
    }

    private static void dataInputTeachers() {
        System.out.print( "Введите фамилию: " );
        String surname = scanner.next();
        System.out.print( "Введите год рождения: " );
        int year = scanner.nextInt();
        System.out.print( "Введите стаж работы: " );
        int experience = scanner.nextInt();
        new Teacher( surname, year, experience );
    }
}
