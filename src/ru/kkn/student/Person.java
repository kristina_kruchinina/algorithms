package ru.kkn.student;

import java.util.Date;

/**
 * Класс для представления человека
 *
 * @author K.Kruchinina, 17it17
 */

class Person {
    private String surname;
    private int age;


    Person(String surname, int year){
        this.surname = surname;
        this.age = year;
    }
    Person(){
        this.surname = "неизвестно";
        this.age = 0;
    }

    String getSurname() {
        return surname;
    }

    void setSurname(String surname) {
        this.surname = surname;
    }

    int getAge() {
        Date date = new Date();
        String s = String.format( "%tY",date );
        int thisYear = Integer.parseInt( s );
        return thisYear - age;
    }

    void setAge(int age) {
        this.age = age;
    }


    @Override
    public String toString() {
        return "Person{" + "surname='" + surname + '\'' + ", age=" + age + '}';
    }
}
