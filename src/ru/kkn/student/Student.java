package ru.kkn.student;

/**
 * Класс для представления студента
 *
 * @author K.Kruchinina, 17it17
 */

public class Student extends Person {
    private String group;
    private int admissionYear;
    private String specialtyCode;

    Student(String surname, int year, String group, int admissionYear, String specialtyCode) {
        super( surname, year );
        this.group = group;
        this.admissionYear = admissionYear;
        this.specialtyCode = specialtyCode;
    }

    Student() {
        group = "название группы";
        admissionYear = 0;
        specialtyCode = "00.00.00";
    }


    void setGroup(String group) {
        this.group = group;
    }


    void setAdmissionYear(int admissionYear) {
        this.admissionYear = admissionYear;
    }


    public void setSpecialtyCode(String specialtyCode) {
        this.specialtyCode = specialtyCode;
    }

    public String getGroup() {
        return group;
    }

    public int getAdmissionYear() {
        return admissionYear;
    }

    public String getSpecialtyCode() {
        return specialtyCode;
    }

    @Override
    public String toString() {
        return "Student{" + " surname = " + getSurname()+ " year = "
                + getAge() + " group = " + group + '\'' + ", admissionYear = "
                + admissionYear + ", specialtyCode = " + specialtyCode + '}';
    }
}
