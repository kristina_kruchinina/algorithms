package ru.kkn.seabattle;
import java.util.ArrayList;

/**
 * Класс для представления кораблей
 *
 * @author K.Kruchinina, 17it17
 */
public class Ship {
    private ArrayList <Integer> ship = new ArrayList <>();
    private ArrayList<Integer> shots = new ArrayList<>();

    Ship(int coordinate){
        ship.add( coordinate );
        ship.add( ++coordinate );
        ship.add( ++coordinate );
    }
    /**
     * Возвращает результат выстрела игрока:
     * если попал - true, если мимо - false
     *
     * @param shot выстрел
     * @return результат выстрела игрока:
     * если попал - true, если мимо - false
     */
    boolean shooting(int shot) {
        if (shots.contains(shot)) {
            System.out.println("Уже было попадаение по этой координате!");
        }
        if (!ship.contains(shot)) {
            System.out.println("Мимо!");
            shots.add(shot);
        } else {
            int tmp = ship.indexOf(shot);
            ship.remove(tmp);
            shots.add(shot);
            System.out.println("Попал!");
        }

        if (ship.isEmpty()) {
            System.out.println("Убил!");
        }
        return ship.isEmpty();
    }

    @Override
    public String toString() {
        return "Корабль " + ship;
    }
}
