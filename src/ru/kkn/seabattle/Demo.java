package ru.kkn.seabattle;

import java.util.Scanner;

/**
 * Класс для представления работоспособностии Ship
 *
 * @author Kruchinina Kristina, 17it17
 */
class Demo {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Ship ship = new Ship((int) (Math.random() * 7));
        System.out.println(ship);
        System.out.println("Вы использовали " + attempts(ship) + " попыток");
    }

    /**
     * Возвращает количество попыток при стрельбе по кораблю
     *
     * @param ship корабль
     * @return количество попыток при стрельбе по кораблю
     */
    private static int attempts(Ship ship) {
        int tunnel = 10;
        boolean result;
        int attempts = 0;
        do {
            System.out.print("Введите координату: " );
            int shot = scanner.nextInt();
            while(shot > tunnel || shot < 0){
                System.out.print("Вы вышли за границы тоннеля! Введите другую координату: ");
                shot = scanner.nextInt();
            }
            result = ship.shooting(shot);
            attempts++;
        } while (!result);
        return attempts;
    }
}

