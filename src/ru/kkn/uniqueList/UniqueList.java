package ru.kkn.uniqueList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Класс для представления уникальных значений
 *
 * @author K. Kruchinina, 17it17
 */

public class UniqueList {
    public static void main(String[] args) {
        ArrayList <String> list = new ArrayList<>();
        list.add( "a" );
        list.add( "b" );
        list.add( "b" );
        ArrayList <Integer> list1 = new ArrayList <>();
        list1.add( 1 );
        list1.add( 2 );
        list1.add( 3 );
        list1.add( 2 );
        Set<String> set = new HashSet<>( list );
        Set<Integer> set1 = new HashSet <>( list1 );
        System.out.println(set);
        System.out.println(set1);
    }
}