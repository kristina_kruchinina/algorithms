package ru.kkn.nestedwithenum;

/**
 * Пример вложенного класса-перечисления
 *
 */

public class Human {
    private String name;
    private int age;
    private Relations relations;

    enum Relations {
        SINGLE,
        IN_LOVE,
        ACTIVE_SEARCH,
        MARRIED,
        DIVORCED
    }

    Human(String name, int age, Relations relations) {
        this.name = name;
        this.age = age;
        this.relations = relations;
    }

    public Relations getRelations() {
        return relations;
    }

    void setRelations(Relations relations) {
        this.relations = relations;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", relations=" + relations +
                '}';
    }
}