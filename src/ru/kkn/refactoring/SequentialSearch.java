package ru.kkn.refactoring;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Реализация последовательного поиска в массиве целых чисел
 *
 * @author K.Kruchinina, 17it17
 */

public class SequentialSearch {
    private static Scanner scanner = new Scanner( System.in );

    public static void main(String[] args) {
        int[] arrayOfNumbers = new int[arrayDimension()];
        random(arrayOfNumbers);
        System.out.println(Arrays.toString(arrayOfNumbers));
        outputMessage(sequentialSearch(arrayOfNumbers,inputNumber()));
    }

    /**
     * Возвращает индекс элемента,
     * если число {@code number} найдено
     * или -1, если числа в массиве нет
     *
     * @param array массив целых чисел
     * @param num целое число
     * @return индекс элемента,
     * если число найдено или -1,
     * если числа в массиве нет
     */
    private static int sequentialSearch(int[] array, int num) {
        for (int index = 0; index < array.length; index++) {
            if (array[index] == num) {
                return index;
            }
        }
        return -1;
    }

    /**
     * Возвращает сообщение "нет такого значения",
     * если элемента нет в массиве
     * и номер элемента в последовательности,
     * если элемент найден
     *
     * @param index индекс массива
     */

    private static void outputMessage(int index) {
        System.out.println(index == -1 ? "нет такого значения" :
                "номер элемента в последовательности" + index);
    }

    /**
     * Возврашает массив рандомных целых чисел
     *
     * @param array массив рандомных чисел
     */
    private static void random(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = -25 + (int) (Math.random() * 50 + 1);
        }
    }

    /**
     * Возвращает размерность массива
     *
     * @return размерность массива
     */

    private static int arrayDimension() {
        System.out.print("Введите размерность массива: ");
        return scanner.nextInt();
    }

    /**
     * Возвращает введенное число
     *
     * @return введенное число
     */
    private static int inputNumber() {
        System.out.print("Введите число: ");
        return scanner.nextInt();
    }
}

