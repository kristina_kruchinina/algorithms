package ru.kkn.randomizer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Рандомайзер
 * Поочередно выводит фамилии студентов в рандомном
 * порядке до тех пор, пока список не окажется пуст.
 *
 * @author K.Kruchinina
 */

public class Randomizer {
    public static void main(String[] args) throws IOException {
        List <String> list = Files.readAllLines( Paths.get( "src/ru/kkn/randomizer/list.txt" ) );
        studentList( list );
        System.out.println( Arrays.toString( new List[]{list} ) );
    }

    /**
     * Возврашает рандомный заполненный массив студентов
     *
     * @param list массив студентов
     */
    private static void studentList(List <String> list) {
        Random rnd = new Random();
        for (int i = list.size() - 1; i > 0; i--) {
            int index = rnd.nextInt( i + 1 );
            String tmp = list.get( index );
            list.set( index, list.get( i ) );
            list.set( i, tmp );
        }
    }
}
