package ru.kkn.bank;

import java.util.Scanner;

/**
 * Класс для представления работоспособности Account
 *
 * @author K.Kruchinina, 17it17
 */

public class Demo {
    private static Scanner scanner = new Scanner( System.in );

    public static void main(String[] args) {
        final Account SBERBANK = new Account( "001", "Kristina Kruchinina" );
        final Account VTB = new Account( "002", "Kruchinina Kristina" );

        final Account.Card SB_VISA = SBERBANK.new Card( "4325 2434 9878 2441" );
        final Account.Card SB_MIR = SBERBANK.new Card( "1232 4633 3532 5676" );
        final Account.Card VTB_MASTERCARD = VTB.new Card( "1083 3859 2413 9567" );

        Account.Card[] cards = {SB_MIR, SB_VISA, VTB_MASTERCARD};
        Account.Card card = cardSelection( cards );


        cardSelection( cards );
        printMenu();
        switchMenu( card, cards );
    }

    /**
     * Поиск аккаунта по номеру карты
     *
     * @param cards массив карт
     * @return
     */
    private static Account.Card cardSelection(Account.Card[] cards) {
        System.out.print( "Втавьте карту: " );
        //Ввод номера карты
        String num = scanner.nextLine();
        for (Account.Card card : cards) {
            if (num.equals( card.getNumber() )) {
                return card;
            } else {
                System.out.println( "Карта не найдена.\n" + "Операция завершена." );
            }
        }
        return null;
    }

    /**
     * Меню аккаунта
     */
    private static void printMenu() {
        System.out.println( "" + "Нажмите:\n" + "1 - просмотр баланса\n" + "2 - пополнение\n" + "3 - снятие\n" + "4 - смена карты\n" + "5 - выход" );
    }

    /**
     * SwitchMenu
     *
     * @param card  карта, с которой работает пользователь
     * @param cards массив карт
     */
    private static void switchMenu(Account.Card card, Account.Card[] cards) {
        String num = scanner.next();
        switch (num) {
            case "1":
                System.out.print( "На вашем счету: " );
                break;
            case "2":
                System.out.print( "Введите сумму для пополнения баланса: " + card.deposit( scanner.nextInt() ) );
                break;
            case "3":
                System.out.print( "Введите сумму для снятия с баланса: " + card.withdraw( scanner.nextInt() ) );
                break;
            case "4":
                System.out.println( cardSelection( cards ) );
                break;
            case "5":
                System.out.print( "Выход" );
                System.exit( 0 );
        }
        switchMenu( card, cards );
    }
}
