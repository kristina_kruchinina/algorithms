package ru.kkn.bank;

/**
 * Класс для предстваления работоспособности аккаунта
 *
 * @author K.Kruchinina, 17it17
 */
class Account {
    private final String number;
    private final String owner;
    private long amount;

    Account(final String number, final String owner) {
        this.number = number;
        this.owner = owner;
    }

    String getNumber() {
        return number;
    }

    String getOwner() {
        return owner;
    }

    long getAmount() {
        return amount;
    }

    private long withdraw(long amountToWithdraw) {
        if (amountToWithdraw < 0) {
            return 0;
        }
        if (amountToWithdraw > amount) {
            final long amountToReturn = amount;
            amount = 0;
            return amountToReturn;
        }
        return amount -= amountToWithdraw;
    }

    private long deposit(long amountToInput) {
        if (amountToInput < 0) {
            return 0;
        }
        return amount += amountToInput;
    }

    /**
     * Внутренний класс Card
     */
    class Card {
        private final String number;

        Card(final String number) {
            this.number = number;
        }

        String getNumber() {
            return number;
        }


        long withdraw(final long amountToWithdraw) {
            return Account.this.withdraw( amountToWithdraw );
        }

        long deposit(final long amountToDeposit) {
            return Account.this.deposit( amountToDeposit );
        }
    }

}
