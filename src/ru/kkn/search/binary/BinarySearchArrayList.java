package ru.kkn.search.binary;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс последовательного поиска в ArrayList массиве целых чисел
 *
 * @author K.Kruchinina, 17it17
 */

public class BinarySearchArrayList {
    private static Scanner scanner = new Scanner( System.in );

    public static void main(String[] args) {
        ArrayList <Integer> integers = new ArrayList <>();
        fillArrayList( integers );
        System.out.println( integers );
        System.out.println( "Введите значение, которое надо найти: " );
        int value = scanner.nextInt();
        int index = binarySearch( integers, value );
        if (index != -1) {
            System.out.println( "Индекс " + value + " равен " + index );
        } else {
            System.out.println( "Значения " + value + "в списке нет" );
        }
    }

    /**
     * Добавляет элементы в массив
     *
     * @param integers ссылка на массив
     */
    private static void fillArrayList(ArrayList <Integer> integers) {
        int amount = 1 + (int) (Math.random() * 20);
        for (int i = 0; i < amount; i++) {
            integers.add( i );
        }
    }

    /**
     * Возвращает индекс элемента, если число {@code number} найдено
     * или -1, если искомого числа в массиве нет
     *
     * @param integers массив целых чисел
     * @param value    искомое целое число
     * @return индекс элемента, если число найдено или -1, если искомого числа в массиве нет
     */

    private static int binarySearch(ArrayList <Integer> integers, int value) {
        int low = 0;
        int high = integers.size() - 1;
        while (low <= high) {
            int mid = (high + low) / 2;
            if (integers.get( mid ) == value) {
                return mid;
            }
            if (value < integers.get( mid )) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return -1;
    }
}
