package ru.kkn.search.sequential;

/**
 * Реализация последовательного поиска в массиве целых чисел
 *
 * @author K.Kruchinina, 17it17
 */

public class SequentialSearch {
    public static void main(String[] args) {
        int arr[] = {1, 2, 10, 18, 20, 38, 100};
        System.out.println( sequentialSearch( arr, 10) );
        System.out.println( sequentialSearch( arr,3 ));
    }

    /**
     * Возвращает индекс элемента, если число {@code number} найдено
     * или -1, если искомого числа в массиве нет
     *
     * @param array массив целых чисел
     * @param num искомое целое число
     * @return индекс элемента, если число найдено или -1, если искомого числа в массиве нет
     */

    private static int sequentialSearch(int[] array, int num) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == num) {
                return i;
            }
        }
        return -1;
    }
}
