package ru.kkn.guessthenumber;

import java.util.Scanner;

/**
 * Класс для поиска рандомного числа в заданном диапазоне.
 *
 * @author K. Kruchinina, 17it17
 */

public class GuessTheNumber {
    private static Scanner scanner = new Scanner( System.in );

    public static void main(String[] args) {
        System.out.print( "Введите диапазон: " );
        int min = scanner.nextInt();
        int max = scanner.nextInt();
        int numRandom = min + (int) (Math.random() * max);
        search( numRandom );
    }

    /**
     * Поиск рандомного числа (игра)
     *
     * @param numRandom рандомное число
     */
    private static void search(int numRandom) {
        int retryCount = 0, numEntered;
        do {
            retryCount++;
            System.out.print( "Введите число: " );
            numEntered = scanner.nextInt();
            System.out.println( numRandom > numEntered ? "Больше" : "Меньше" );
            if (numRandom == numEntered) {
                System.out.println( "Бинго! Вы угадали" );
            }
        } while (numRandom != numEntered);
        System.out.println( "Количество попыток: " + retryCount );
    }
}