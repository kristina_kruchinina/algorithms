package ru.kkn.patterns.observer.publisher;

import java.util.ArrayList;

public class Publisher extends PublisherInterface {
    private ArrayList<PublisherActionListener> listeners = new ArrayList<>();

    public void addListener(PublisherActionListener listener) {
        listeners.add(listener);
    }

    public void removeListener(PublisherActionListener listener) {
        listeners.remove(listener);
    }

    public void removeAllListeners() {
        listeners.clear();
    }

    @Override
    public void notifySubscribers(String message) {
        for (PublisherActionListener actionListener : listeners) {
            actionListener.doAction();
        }
    }

    public void createNewMessage(String message) {
        System.out.println("Издатель напечатал сообщение: " + message);
        notifySubscribers(message);
    }
}