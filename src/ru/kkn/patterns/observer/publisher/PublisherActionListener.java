package ru.kkn.patterns.observer.publisher;

public abstract class PublisherActionListener {
    void doAction() {
    }
}
