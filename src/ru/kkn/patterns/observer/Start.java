package ru.kkn.patterns.observer;

import ru.kkn.patterns.observer.publisher.Publisher;
import ru.kkn.patterns.observer.subscribers.Subscriber1;
import ru.kkn.patterns.observer.subscribers.Subscriber2;

public class Start {
    public static void main(String[] args) {
        Subscriber1 subscriber1 = new Subscriber1();
        Subscriber2 subscriber2 = new Subscriber2();

        Publisher publisher = new Publisher();

        publisher.addListener(subscriber1);
        publisher.addListener(subscriber2);

        publisher.removeAllListeners();

        publisher.removeListener(subscriber1);
        publisher.removeListener(subscriber2);

        publisher.createNewMessage("Пам Пам!");
    }
}