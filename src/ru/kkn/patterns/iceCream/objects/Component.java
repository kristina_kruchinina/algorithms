package ru.kkn.patterns.iceCream.objects;

public interface Component {
    void cooking();
}