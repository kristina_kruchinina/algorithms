package ru.kkn.patterns.iceCream.objects;

public class IceCream implements Component {

    @Override
    public void cooking() {
        System.out.println("Мороженное: ");
    }
}
