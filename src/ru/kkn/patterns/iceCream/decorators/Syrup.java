package ru.kkn.patterns.iceCream.decorators;

import ru.kkn.patterns.iceCream.objects.Component;

/**
 * Класс для добавления сиропа
 */

public class Syrup extends Decorator {
    public Syrup(Component component) {
        super(component);
    }

    @Override
    public void afterCooking() {
        System.out.println("добавлен сироп");
    }
}