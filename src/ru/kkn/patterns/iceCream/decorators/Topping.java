package ru.kkn.patterns.iceCream.decorators;

import ru.kkn.patterns.iceCream.objects.Component;

/**
 * Класс для добавления топпинга
 *
 * @author K.Kruchinina, 17it17
 */

public class Topping extends Decorator {
    public Topping(Component component) {
        super(component);
    }

    @Override
    public void afterCooking() {
        System.out.println("добавлен топпинг");
    }
}
