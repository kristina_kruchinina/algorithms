package ru.kkn.patterns.iceCream.decorators;

import ru.kkn.patterns.iceCream.objects.Component;

/**
 * Класс декоратор
 *
 * @author K.Kruchinina, 17it17
 */

public abstract class Decorator implements Component {
    private Component component;

    Decorator(Component component) {
        this.component = component;
    }

    abstract void afterCooking();

    @Override
    public void cooking() {
        component.cooking();
        afterCooking();
    }
}
