package ru.kkn.patterns.iceCream;

import ru.kkn.patterns.iceCream.decorators.Syrup;
import ru.kkn.patterns.iceCream.decorators.Topping;
import ru.kkn.patterns.iceCream.objects.Component;
import ru.kkn.patterns.iceCream.objects.IceCream;
import ru.kkn.patterns.iceCream.objects.Smoothies;
import ru.kkn.patterns.iceCream.objects.Yogurt;

import java.util.Random;

/**
 * Демонстрация работоспособности паттерна с мороженым
 *
 * @author K.Kruchinina, 17it17
 */

public class Demo {
    public static void main(String[] args) {
        Component iceCream;
        Component smoothies;
        Component yogurt;
        Random random = new Random();

        boolean addSyrup = random.nextBoolean();
        if (addSyrup) {
            iceCream = new Syrup(new IceCream());
            smoothies = new Syrup(new Smoothies());
            yogurt = new Syrup(new Yogurt());
        } else {
            iceCream = new IceCream();
            smoothies = new Smoothies();
            yogurt = new Yogurt();
        }

        iceCream.cooking();
        smoothies.cooking();
        yogurt.cooking();

        System.out.println();

        boolean addTopping = random.nextBoolean();
        if (addTopping) {
            iceCream = new Topping(new IceCream());
            smoothies = new Topping(new Smoothies());
            yogurt = new Topping(new Yogurt());
        } else {
            iceCream = new IceCream();
            smoothies = new Smoothies();
            yogurt = new Yogurt();
        }

        iceCream.cooking();
        smoothies.cooking();
        yogurt.cooking();

        System.out.println();

        iceCream = new Topping(new Syrup(new IceCream()));
        iceCream.cooking();

        smoothies = new Topping(new Syrup(new Smoothies()));
        smoothies.cooking();

        yogurt = new Topping(new Syrup(new Yogurt()));
        yogurt.cooking();
    }
}

