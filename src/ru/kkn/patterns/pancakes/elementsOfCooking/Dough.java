package ru.kkn.patterns.pancakes.elementsOfCooking;

import ru.kkn.patterns.pancakes.IReady;

public class Dough implements IReady {

    public void ready() {
        System.out.println("Тесто готово!");
    }
}