package ru.kkn.patterns.pancakes.elementsOfCooking;

import ru.kkn.patterns.pancakes.IReady;

public class Dishes implements IReady {

    public void ready() {
        System.out.println("Посуда готова!");
    }
}