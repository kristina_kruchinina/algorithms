package ru.kkn.patterns.pancakes;

import java.util.Scanner;

public class Demo {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        CookingPancakes cookingPancakes = new CookingPancakes();
        System.out.print("Введите количество порций: ");
        int amount = scanner.nextInt();
        cookingPancakes.cookingPancakes(amount);
    }
}
