package ru.kkn.patterns.pancakes;

import ru.kkn.patterns.pancakes.elementsOfCooking.Dishes;
import ru.kkn.patterns.pancakes.elementsOfCooking.Dough;
import ru.kkn.patterns.pancakes.elementsOfCooking.Pancake;

class CookingPancakes {
    private Dough dough = new Dough();
    private Dishes dishes = new Dishes();
    private Pancake pancake = new Pancake();

    void cookingPancakes(int amount) {
        dough.ready();
        dishes.ready();
        for (int i = 1; i <= amount; i++) {
            pancake.ready();
        }
    }
}