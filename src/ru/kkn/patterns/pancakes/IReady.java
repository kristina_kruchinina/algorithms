package ru.kkn.patterns.pancakes;

public interface IReady {
    void ready();
}
