package ru.kkn.sorting.selection;

import java.util.ArrayList;

/**
 * Реализация сортировки выбором в массиве ArrayList целых чисел
 *
 * @author K. Kruchinina, 17it17
 */
public class SelectionSortArrayList {
    public static void main(String[] args) {
        ArrayList <Integer> integers = new ArrayList <>();
        fillArrayList( integers );
        System.out.println( integers );
        selectionSort( integers );
        System.out.println( integers );
    }

    /**
     * Заполняет массив {@code array} рандомными числами
     *
     * @param integers ArrayList целых чисел
     */
    private static void fillArrayList(ArrayList <Integer> integers) {
        int amount = 1 + (int) (Math.random() * 20);
        for (int i = 0; i < amount; i++) {
            integers.add( 1 + (int) (Math.random() * 20) );
        }
    }

    /**
     * Сортирует массив {@code array} по возрастанию значений
     *
     * @param integers ArrayList целых чисел
     */
    private static void selectionSort(ArrayList <Integer> integers) {
        for (int out = 0; out < integers.size() - 1; out++) {
            int indexMin = out;
            for (int in = out + 1; in < integers.size(); in++) {
                if (integers.get( in ) < integers.get( indexMin )) {
                    indexMin = in;
                }
                int tmp = integers.get( out );
                integers.set( out, integers.get( indexMin ) );
                integers.set( indexMin, tmp );
            }
        }
    }
}

