package ru.kkn.sorting.inserts;

import java.util.Arrays;

/**
 * Реализация сортировки вставками в массиве целых чисел
 *
 * @author K. Kruchinina, 17it17
 */
public class InsertsSort {
    public static void main(String[] args) {
        int[] arr = {2, 6, 3, 1, 5, 7, 4, 9, 8};
        System.out.println( Arrays.toString( arr ) );
        insert( arr );
        System.out.println( Arrays.toString( arr ) );
    }

    /**
     * Сортирует масиив{@code array} по возрастанию значений
     *
     * @param arr массив целых чисел
     */
    private static void insert(int[] arr) {
        int temp, j;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                temp = arr[i + 1];
                arr[i + 1] = arr[i];
                j = i;
                while (j > 0 && temp < arr[j - 1]) {
                    arr[j] = arr[j - 1];
                    j--;
                }
                arr[j] = temp;
            }
        }
    }
}
