package ru.kkn.sorting.bubble;

import java.util.Arrays;

/**
 * Реализация сортировки пузырьком в массиве целых чисел
 *
 * @author K. Kruchinina, 17it17
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] array = {8, 5, 2, 9, 6, 4, 7, 1, 3};
        System.out.println( Arrays.toString( array ) );
        bubbleSorting( array );
        System.out.println( Arrays.toString( array ) );
    }

    /**
     * Сортирует масиив{@code array} по возрастанию значений
     *
     * @param array массив целых чисел
     */
    private static void bubbleSorting(int[] array) {
        for (int out = array.length - 1; out > 0; out--) {
            for (int in = 0; in < out; in++) {
                if (array[in] > array[in + 1]) {
                    int temp = array[in];
                    array[in] = array[in + 1];
                    array[in + 1] = temp;
                }
            }
        }
    }
}
