package ru.kkn.sorting.bubble;

import java.util.ArrayList;

/**
 * Реализация сортировки пузырьком в массиве ArrayList целых чисел
 *
 * @author K. Kruchinina, 17it17
 */
public class BubbleSortArrayList {
    public static void main(String[] args) {
        ArrayList <Integer> integers = new ArrayList <>();
        fillArrayList( integers );
        System.out.println( integers );
        bubbleSort( integers );
        System.out.println( integers );
    }

    /**
     * Заполняет массив {@code array} рандомными числами
     *
     * @param integers ArrayList целых чисел
     */
    private static void fillArrayList(ArrayList <Integer> integers) {
        int amount = 1 + (int) (Math.random() * 20);
        for (int i = 0; i < amount; i++) {
            integers.add( (int) (1 + (Math.random() * 20)) );
        }
    }

    /**
     * Сортирует массив {@code array} по возрастанию значений
     *
     * @param integers ArrayList целых чисел
     */
    private static void bubbleSort(ArrayList <Integer> integers) {
        for (int out = integers.size() - 1; out > 0; out--) {
            for (int in = 0; in < out; in++) {
                if (integers.get( in ) > integers.get( in + 1 )) {
                    int temp = integers.get( in );
                    integers.set( in, integers.get( in + 1 ) );
                    integers.set( in + 1, temp );
                }
            }
        }
    }
}

