package ru.kkn.fibonaccinumbers;

/**
 * Числа Фибоначчи
 * <ol>
 * <li>Нахождение n-го числа Фибоначчи итеративным способом</li>
 * <li>Нахождение n-го числа Фибоначчи рекурсивным способом</li>
 * </ol>
 *
 * @author K.Kruchinina, 17it17
 */

public class Primitive {
    public static void main(String[] args) {
        iterative( 46 );

        for (int i = 0; i < 46; ++i) {
            System.out.print( recursive( i ) + " " );
        }
    }

    /**
     * числа Фибоначчи
     *
     * @param n порядковый номер числа Фибоначчи
     */
    private static void iterative(int n) {
        int[] f = new int[n];
        f[0] = 0;
        f[1] = 1;
        for (int i = 2; i < n; ++i) {
            f[i] = f[i - 1] + f[i - 2];
        }
        for (int i = 0; i < n; ++i) {
            System.out.print( f[i] + " " );
        }
    }

    /**
     * Рекурсивный метод возвращает число Фибоначчи
     *
     * @param n порядоковый номер числа Фибоначчи
     * @return число Фибоначчи
     */
    private static int recursive(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return recursive( n - 1 ) + recursive( n - 2 );
        }
    }
}


