package ru.kkn.fibonaccinumbers;

/**
 * Числа Фибоначчи
 * <ol>
 * <li>Нахождение n-го числа Фибоначчи итеративным способом</li>
 * <li>Нахождение всех чисел в заданом диапазоне</li>
 * </ol>
 *
 * @author K.Kruchinina, 17it17
 */
public class BigInteger {
    public static void main(String[] args) {
        java.math.BigInteger[] numbers = new java.math.BigInteger[100];
        System.out.println( searchNumber( 10 ) );
        fibonacciNumber( numbers );

    }

    /**
     * Нахождение числа Фибоначчи итеративным способом
     *
     * @param n число
     */
    private static java.math.BigInteger searchNumber(int n) {
        java.math.BigInteger num1 = java.math.BigInteger.valueOf( 0 );
        java.math.BigInteger num2 = java.math.BigInteger.valueOf( 1 );
        for (int i = 2; i <= n; ++i) {
            java.math.BigInteger next = num1.add( num2 );
            num1 = num2;
            num2 = next;
        }
        return num2;
    }


    /**
     * Нахождение всех чисел в заданом диапазоне
     *
     * @param num число
     */
    private static void fibonacciNumber(java.math.BigInteger[] num) {
        num[0] = java.math.BigInteger.valueOf( 0 );
        num[1] = java.math.BigInteger.valueOf( 1 );
        for (int i = 2; i < num.length; ++i) {
            num[i] = num[i - 1].add( num[i - 2] );
        }

        for (java.math.BigInteger aNum : num) {
            System.out.println( aNum );
        }
    }
}
